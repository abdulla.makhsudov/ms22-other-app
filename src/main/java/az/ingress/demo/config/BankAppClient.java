package az.ingress.demo.config;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "bank-app", url = "http://localhost:8085/bank-app", configuration = Test.class)
public interface BankAppClient {

    @GetMapping("/accounts/v1")
    String getToken(@RequestParam String name);

    @GetMapping("/test")
    String get();
}
