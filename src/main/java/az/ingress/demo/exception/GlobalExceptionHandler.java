package az.ingress.demo.exception;

import feign.FeignException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<ErrorDiteails> handleDataNotFoundException(DataNotFoundException exception,
                                                                     HttpServletRequest request,
                                                                     HttpServletResponse response) {
        ErrorDiteails errorDiteails = ErrorDiteails.builder()
                .errorMessage(exception.getMessage())
                .path(request.getRequestURI())
                .status(404)
                .timestamp(String.valueOf(LocalDateTime.now()))
                .build();
        return ResponseEntity.status(errorDiteails.getStatus()).body(errorDiteails);
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<ErrorDiteails> handleFeignException(FeignException exception,
                                                                     HttpServletRequest request,
                                                                     HttpServletResponse response) {
        ErrorDiteails errorDiteails = ErrorDiteails.builder()
                .errorMessage(exception.getMessage())
                .path(request.getRequestURI())
                .status(488)
                .timestamp(String.valueOf(LocalDateTime.now()))
                .build();
        return ResponseEntity.status(errorDiteails.getStatus()).body(errorDiteails);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDiteails> handleDataNotFoundException(RuntimeException exception,
                                                                     HttpServletRequest request,
                                                                     HttpServletResponse response) {
        ErrorDiteails errorDiteails = ErrorDiteails.builder()
                .errorMessage("General exception")
                .path(request.getRequestURI())
                .status(404)
                .timestamp(String.valueOf(LocalDateTime.now()))
                .build();
        return ResponseEntity.status(errorDiteails.getStatus()).body(errorDiteails);
    }
}
