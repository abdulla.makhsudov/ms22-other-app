package az.ingress.demo.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorDiteails {
    private String errorMessage;
    private int status;
    private String path;
    private String timestamp;
    @JsonIgnore
    private String failMessage;
    @JsonIgnore
    private String failCode;
}
