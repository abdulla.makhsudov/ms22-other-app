package az.ingress.demo.exception;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import feign.Response;
import feign.codec.ErrorDecoder;
import java.io.IOException;
import java.util.MissingFormatArgumentException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FeignCustomErrorHandler implements ErrorDecoder {
    private final ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {
        try {
            var bodyIs = response.body().asInputStream();
            ErrorDiteails errorDetails = new ObjectMapper().readValue(bodyIs, ErrorDiteails.class);

            switch (response.status()) {
                case 415:
                    return new MissingFormatArgumentException(errorDetails.getErrorMessage());
//                case 404:
//                    return new RuntimeException(errorDetails.getErrorMessage());
                default:
                    return errorDecoder.decode(methodKey, response);
            }

        } catch (StreamReadException e) {
            throw new RuntimeException(e);
        } catch (DatabindException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


}
