package az.ingress.demo.controller;

import az.ingress.demo.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TokenController {

    private final TokenService tokenService;

    @GetMapping("/token")
    public ResponseEntity<String> getToken(@RequestParam("name") String name) {
        return ResponseEntity.ok(tokenService.getToken(name));
    }

    @GetMapping("/test")
    public ResponseEntity<String> getToken() {
        return ResponseEntity.ok(tokenService.get());
    }

}
