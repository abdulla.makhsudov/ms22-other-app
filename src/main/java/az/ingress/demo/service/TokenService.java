package az.ingress.demo.service;

import az.ingress.demo.config.BankAppClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TokenService {
    private final BankAppClient bankAppClient;

    public String getToken(String name) {
        return bankAppClient.getToken(name);
    }

    public String get() {
        return bankAppClient.get();
    }
}
